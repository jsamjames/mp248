import numpy as np
#creating the Gaussian Function and calculating f(x) from an input x value
def gauss(x,a=1,b=0,c=1):
    return a*np.exp(-((x-b)**2)/(2*(c**2)))
x = float(input("x = "))
print("f(x) = " +str(gauss(x)))